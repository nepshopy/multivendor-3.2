<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('guest_id')->nullable();
            $table->longText('shipping_address')->nullable();
            $table->string('payment_type',20)->nullable();
            $table->string('payment_status',20)->default('unpaid');
            $table->longText('payment_details')->nullable();
            $table->double('grand_total',10,2)->nullable();
            $table->double('coupon_discount',10,2)->default('0.00');
            $table->mediumText('code')->nullable();
            $table->dateTime('date');
            $table->integer('viewed')->default('0');
            $table->integer('delivery_viewed')->default('1');
            $table->integer('payment_status_viewed')->default('1');
            $table->integer('commission_calculated')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
