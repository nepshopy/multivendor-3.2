<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200);
            $table->string('added_by',10);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('category_id');
            $table->integer('subcategory_id');
            $table->integer('subsubcategory_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('photos',2000)->nullable();
            $table->string('attribute_image',2000)->nullable();
            $table->string('	thumbnail_img',100)->nullable();
            $table->string('	video_provider',20)->nullable();
            $table->string('	video_link',100)->nullable();
            $table->mediumText('tags')->nullable();
            $table->longText('description')->nullable();
            $table->double('unit_price', 10,2);
            $table->double('purchase_price', 10,2);
            $table->integer('variant_product')->default('0');
            $table->string('attributes',1000)->default('[]');
            $table->mediumText('choice_options')->nullable();
            $table->mediumText('colors')->nullable();
            $table->text('variations')->nullable();
            $table->integer('todays_deal')->default('0');
            $table->integer('published')->default('1');
            $table->integer('featured')->default('0');
            $table->integer('current_stock')->default('0');
            $table->string('unit',20)->nullable();
            $table->integer('min_qty')->default('1');
            $table->double('discount',10,2)->nullable();
            $table->string('discount_type',10)->nullable();
            $table->double('tax',10,2)->nullable();
            $table->string('tax_type',10)->nullable();
            $table->string('shipping_type',20)->default('flat_rate');
            $table->double('shipping_cost',10,2)->default('0.00');
            $table->integer('number_of_sale')->default('0');
            $table->mediumText('meta_title')->nullable();
            $table->longText('meta_description')->nullable();
            $table->string('meta_img',255)->nullable();
            $table->string('pdf',255)->nullable();
            $table->mediumText('slug');
            $table->double('rating', 10,2);
            $table->string('barcode', 255)->default('0.00');
            $table->integer('digital')->default('0');
            $table->string('file_name',255)->nullable();
            $table->string('file_path',255)->nullable();
            $table->enum('product_display',['bulk','single','both'])->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
