<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->integer('published')->default('0');
            $table->integer('status')->default('0');
            $table->string('added_by',50)->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->integer('subcategory_id')->nullable();
            $table->integer('subsubcategory_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('photos', 255)->nullable();
            $table->string('thumbnail_img', 150)->nullable();
            $table->string('condition', 50)->nullable();
            $table->text('location')->nullable();
            $table->string('video_provider', 100)->nullable();
            $table->string('video_link', 200)->nullable();
            $table->string('unit', 200)->nullable();
            $table->string('tags', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->double('unit_price',28,2)->default('0.00');
            $table->string('meta_title', 200)->nullable();
            $table->string('meta_description', 500)->nullable();
            $table->string('meta_img', 200)->nullable();
            $table->string('pdf', 200)->nullable();
            $table->string('slug', 200)->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_products');
    }
}
