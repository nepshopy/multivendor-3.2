<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referred_by')->nullable();
            $table->integer('provider_id')->nullable();
            $table->string('user_type')->default('customer');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('new_email_verification_code')->nullable();
            $table->text('verification_code')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->text('avatar')->nullable();
            $table->text('avatar_original')->nullable();
            $table->text('address')->nullable();
            $table->text('country')->nullable();
            $table->text('city')->nullable();
            $table->text('postal_code')->nullable();
            $table->text('phone')->nullable();
            $table->longText('balance')->nullable();
            $table->text('referral_code')->nullable();
            $table->integer('customer_package_id')->nullable();
            $table->integer('remaining_uploads')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
