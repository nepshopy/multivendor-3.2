<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('frontend_color',255);
            $table->string('logo',255)->nullable();
            $table->string('admin_logo',255)->nullable();
            $table->string('admin_login_background',255)->nullable();
            $table->string('admin_login_sidebar',255)->nullable();
            $table->string('favicon',255)->nullable();
            $table->string('site_name',255)->nullable();
            $table->string('address',1000)->nullable();
            $table->mediumText('description');
            $table->string('phone',100)->nullable();
            $table->string('email',255)->nullable();
            $table->string('facebook',1000)->nullable();
            $table->string('instagram',1000)->nullable();
            $table->string('twitter',1000)->nullable();
            $table->string('youtube',1000)->nullable();
            $table->string('google_plus',1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_settings');
    }
}
