<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('seller_id');
            $table->unsignedInteger('product_id');
            $table->longText('variation')->nullable();
            $table->double('price',10,2)->nullable();
            $table->double('tax',10,2)->default('0.00');
            $table->double('shipping_cost',10,2)->default('0.00');
            $table->integer('quantity')->nullable();
            $table->string('payment_status',10)->default('unpaid');
            $table->string('delivery_status',20)->default('pending');
            $table->string('shipping_type',255)->nullable();
            $table->integer('pickup_point_id')->nullable();
            $table->string('product_referral_code',255)->nullable();
            $table->timestamps();

            $table->foreign('seller_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
