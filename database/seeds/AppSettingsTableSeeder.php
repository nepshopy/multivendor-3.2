<?php

use Illuminate\Database\Seeder;
use App\AppSetting;

class AppSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $app = new AppSetting;
        $app->name = 'Nepshopy';
        $app->logo = 'mylogo.jpg';
        $app->currency_id = '1';
        $app->currency_format = 'symbol';
        $app->facebook = 'https://facebook.com';
        $app->twitter = 'https://twitter.com';
        $app->instagram = 'https://instagram.com';
        $app->youtube = 'https://youtube.com';
        $app->google_plus = 'https://googleplus.com';
        $app->save();
    }
}
