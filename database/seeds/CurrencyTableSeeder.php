<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(['name'=>'Nepali Rupee','symbol'=>'Rs','exchange_rate'=>'120','status'=>'1','code'=>'NPR']);
    }
}
