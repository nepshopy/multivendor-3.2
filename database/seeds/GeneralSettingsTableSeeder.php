<?php

use Illuminate\Database\Seeder;
use App\GeneralSetting;

class GeneralSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GeneralSetting::create([
    'frontend_color'=>'default','logo'=>'logo.png','admin_logo'=>'admin.jpg','admin_login_background'=>'background.jpg','admin_login_sidebar'=>'sidebar.jpg','favicon'=>'favicon.png','site_name'=>'nepshopy','address'=>'asdfgh','description'=>'qwertyuioplkjhgfdsazxcvbnm.','phone'=>'1234567890','email'=>'demo@example.com','facebook'=>'facebook','instagram'=>'instagram','twitter'=>'twitter','youtube'=>'youtube','google_plus'=>'google'
    ]);
    }
}
