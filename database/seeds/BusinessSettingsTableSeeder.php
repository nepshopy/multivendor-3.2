<?php

use Illuminate\Database\Seeder;
use App\BusinessSetting;

class BusinessSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessSetting::create([
            'type' => 'home_default_currency',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'system_default_currency',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'currency_format',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'symbol_format',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'no_of_decimals',
            'value' => '3'

        ]);
        BusinessSetting::create([
            'type' => 'product_activation',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'vendor_system_activation',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'show_vendors',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'paypal_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'stripe_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'cash_payment',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'payumoney_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'payumoney_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'best_selling',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'paypal_sandbox',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'sslcommerz_sandbox',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'sslcommerz_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'vendor_commission',
            'value' => '20'

        ]);
        BusinessSetting::create([
            'type' => 'verification_form',
            'value' => '[{"type":"text","label":"Your name"},{"type":"text","label":"Shop name"},{"type":"text","label":"Email"},{"type":"text","label":"License No"},{"type":"text","label":"Full Address"},{"type":"text","label":"Phone Number"},{"type":"file","label":"Tax Papers"}]'

        ]);
        BusinessSetting::create([
            'type' => 'google_analytics',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'facebook_login',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'google_login',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'twitter_login',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'payumoney_payment',
            'value' => '1'

        ]); BusinessSetting::create([
            'type' => 'payumoney_sandbox',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'facebook_chat',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'email_verification',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'wallet_system',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'coupon_system',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'current_version',
            'value' => '3.2'

        ]);
        BusinessSetting::create([
            'type' => 'instamojo_payment',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'instamojo_sandbox',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'razorpay',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'paystack',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'pickup_point',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'maintenance_mode',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'voguepay',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'voguepay_sandbox',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'category_wise_commission',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'conversation_system',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'guest_checkout_active',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'facebook_pixel',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'classified_product',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'pos_activation_for_seller',
            'value' => '1'

        ]);
        BusinessSetting::create([
            'type' => 'shipping_type',
            'value' => 'product_wise_shipping'

        ]);
        BusinessSetting::create([
            'type' => 'flat_rate_shipping_cost',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'shipping_cost_admin',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'payhere_sandbox',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'payhere',
            'value' => '0'

        ]);
        BusinessSetting::create([
            'type' => 'google_recaptcha',
            'value' => '1'

        ]);
    }
}
