<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'user_type'=>'admin','name'=>'admin', 'email'=>'admin@admin.com', 'password'=>bcrypt('123456'), 'address'=>'qwerty', 'city'=>'asdfgh', 'postal_code'=>'123456', 'phone'=>'1234567890', 'country'=>'Nepal'
        ]);
    }
}
