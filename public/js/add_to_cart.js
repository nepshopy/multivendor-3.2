$(document).ready(function () {
    productSummmary();
    var product = $('#detail-product').data('detail');
    var colorsname = $('#detail-product').data('colorname');
    var ids = [];
    ids = JSON.parse(localStorage.getItem("ids"));

    var productsInAddToCart = [];
    productsInAddToCart = JSON.parse(localStorage.getItem("products"));

    if (!productsInAddToCart) {
        productsInAddToCart = [];
    }
    if (!productsInAddToCart) {
        productsInAddToCart = [];

    }
    dataCalculation();

    window.variantSelect = function variantSelect() {
        dataCalculation();
    }


    function dataCalculation() {
        ids = JSON.parse(localStorage.getItem("ids"));

        var attribute = [];
        // if (!['', null, undefined].includes(colorsname)) {
        //     attribute.push(colorsname);
        // }
        var colorPushedData = [];
        $("input:checkbox[class=colorsvaraint]:checked").each(function () {
            colorPushedData.push($(this).data('color'))
        });
        attribute.push(colorPushedData)
        if (product && product.choice_options) {
            $.each(JSON.parse(product.choice_options), function (i, data) {
                if (product.bulk.bulk_attribute_ids && product.bulk.bulk_attribute_ids.includes(data.attribute_id)) {
                    attribute.push(data.values)
                } else {
                    var d = 'attribute_id_' + data.attribute_id.toString();
                    var pushedData = [];
                    $("input:checkbox[class=" + d + "]:checked").each(function () {
                        if (parseInt($(this).data('attrid')) === parseInt(data.attribute_id)) {
                            pushedData.push($(this).val())
                        }
                    });
                    attribute.push(pushedData)
                }
            });
            var combination = [];

            $.each(attribute[0], function (i, a) {
                if (!['', undefined, null].includes(attribute[1])) {
                    $.each(attribute[1], function (a1i, a1) {
                        if (!['', undefined, null].includes(attribute[2])) {
                            $.each(attribute[2], function (a2i, a2) {
                                if (!['', undefined, null].includes(attribute[3])) {
                                    $.each(attribute[3], function (a3i, a3) {
                                        if (!['', undefined, null].includes(attribute[4])) {
                                            $.each(attribute[4], function (a4i, a4) {
                                                if (!['', undefined, null].includes(attribute[5])) {
                                                    $.each(attribute[5], function (a5i, a5) {
                                                        combination.push(a + '-' + a1 + '-' + a2 + '-' + a3 + '-' + a4 + '-' + a5);
                                                    });
                                                } else {
                                                    combination.push(a + '-' + a1 + '-' + a2 + '-' + a3 + '-' + a4);
                                                }
                                            });
                                        } else {
                                            combination.push(a + '-' + a1 + '-' + a2 + '-' + a3);
                                        }
                                    });
                                }
                                combination.push(a + '-' + a1 + '-' + a2);
                            })
                        } else {
                            combination.push(a + '-' + a1);
                        }
                    })

                } else {
                    combination.push(a);
                }
            });

            var no_of_quantities = 0;
            var totalPerSetPrice = 0;

            var table = '<table class="table">' +
                '<thead><tr>' +
                '<th>Variant</th>' +
                '<th>Price</th>' +
                '<th>Quantity</th>' +
                '<th>Total</th>' +
                '</tr>' +
                '</thead><tbody>';

            if (!['', null, undefined].includes(product.stocks)) {
                var total_sum = 0;
                $.each(product.stocks, function (i, data) {
                    if (combinationCheck(data.variant, combination)) {
                        data.selected = '1';
                        var individual_sum = parseFloat(data.bulk_qty) * parseFloat(data.price)
                        no_of_quantities += data.bulk_qty;
                        totalPerSetPrice += individual_sum;

                        // calculation
                        total_sum += individual_sum;
                        table += '<tr class="bulk-variant" ">' +
                            '<td>' + data.variant + '</td>' +
                            '<td>' + data.price + '</td>' +
                            '<td>' + data.bulk_qty + '</td>' +
                            '<td>' + individual_sum + '</td>' +
                            '</tr>';
                    } else {
                        data.selected = '0';
                        var individual_sum = parseFloat(data.bulk_qty) * parseFloat(data.price)
                        table += '<tr>' +
                            '<td>' + data.variant + '</td>' +
                            '<td>' + data.price + '</td>' +
                            '<td>' + data.bulk_qty + '</td>' +
                            '<td>' + individual_sum + '</td>' +
                            '</tr>';
                    }
                });


            }
            table += '<tr>' +
                '<td colspan="3">Total</td>' +
                '<td>' + total_sum + '</td>' +
                '</tr>';
            table += '</tbody></table>';


            // var attr = ['XS', 'M', 'L'];
            // var attr1 = ["1''", "2''"];
            // var attr2 = ['6GB'];


            $('#variantTable').html(table);
            $('#pieces').html(no_of_quantities);
            $('#set-price').html(totalPerSetPrice ? totalPerSetPrice.toFixed(2) : 0);

            product.bulk.over_all_qty = no_of_quantities;
            product.bulk.no_of_sets = $('#qty').val();
            product.bulk.per_set_price = totalPerSetPrice;

        }

        navBarProductItems()
        addItemNumberInCart(ids)
    }


    function navBarProductItems() {
        var addTocartITems = JSON.parse(localStorage.getItem("products"));
        var navHtml = '';

        $.each(addTocartITems, function (i, res) {
            var base_url = $('.base_url').attr('href');
            var product_url = base_url + '/wholesale/product/';
            var image_url = base_url + '/';

            if (!['', undefined, null].includes(JSON.parse(res.photos))) {
                var image = JSON.parse(res.photos)[0];
            }
            navHtml += '<div class="dropdown-cart-items c-scrollbar">' +
                '            <div class="dc-item">' +
                '                <div class="d-flex align-items-center">' +
                '                    <div class="dc-image">' +
                '                        <a href="' + product_url + res.slug + '">' +
                '                            <img' +
                '                                src="' + image_url + image + '"' +
                '                                class="img-fluid lazyload" alt="">' +
                '                        </a>' +
                '                    </div>' +
                '                    <div class="dc-content">' +
                '                        <span' +
                '                            class="d-block dc-product-name text-capitalize strong-600 mb-1">' +
                '                            <a href="' + product_url + res.slug + '">' + res.name +
                '                            </a>' +
                '                        </span>' +
                '                        <span class="dc-quantity">Set of ' + res.bulk.over_all_qty + ' pieces </span>' +
                '                        <span class="dc-price">Price: Rs : ' + res.bulk.per_set_price.toFixed() + '</span>' +
                '                    </div>' +
                '                    <div class="dc-actions">' +
                '                        <button class="remove" data-id="' + res.id + '">' +
                '                            <i class="la la-close"></i>' +
                '                        </button>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '        </div>'
        });
        $('.add-to-cart-product').html(navHtml)
    }

    function combinationCheck(variant, combination) {
        var result = false;
        $.each(combination, function (combintionI, com) {
            if (arrayEquals(variant, com)) {
                result = true
            }
        });
        return result;
    }

    function arrayEquals(a1, b1) {
        let a = a1.split('-');
        a = a.map(res => {
            return res.trim();
        })
        let b = b1.split('-');
        b = b.map(res => {
            return res.replace(/\s/g, '');
        })

        if (Array.isArray(a) &&
            Array.isArray(b) &&
            a.length === b.length) {
            var counter = 0;
            $.each(b, function (i, data) {
                if (a.includes(data)) {
                    counter++
                }
            });
            if (counter === a.length) {
                return true;
            }
            return false;
        }
    }

    function removeAddToCart(id) {
        productsInAddToCart = JSON.parse(localStorage.getItem('products'));

        // console.log(products);
        var ids = JSON.parse(localStorage.getItem("ids"));

        const index = ids.indexOf(id);
        if (index > -1) {
            ids.splice(index, 1);
        }
        productsInAddToCart = productsInAddToCart.filter(function (value, index, arr) {
            return parseInt(value.id) !== parseInt(id);
        });
        if (!productsInAddToCart) {
            productsInAddToCart = [];
        }
        localStorage.setItem("products", JSON.stringify(productsInAddToCart));
        localStorage.setItem("ids", JSON.stringify(ids));
        dataCalculation();
    }


    $(document).on('click', '.add-to-cart', function () {
        if (!ids) ids = [];
        var id = $(this).data('id');

        productsInAddToCart = JSON.parse(localStorage.getItem('products'));
        if (!productsInAddToCart) {
            productsInAddToCart = [];
        }
        // console.log(productsInAddToCart);
        // console.log(product);
        // check whether item is in cart list or not
        if (ids && (ids.includes(id) || ids.includes(id.toString()))) {
            const index = ids.indexOf(id);
            if (index > -1) {
                ids.splice(index, 1);
            }
            productsInAddToCart = productsInAddToCart.filter(function (value, index, arr) {
                return parseInt(value.id) !== parseInt(id);
            });
            productsInAddToCart.push(product)
            ids.push(id);
            alert('This item has updated successfully');
            // snackbar('This Item has already in cart list.','error');
        } else {
            productsInAddToCart.push(product);
            ids.push(id);
            alert('Item has added successfully in cart list.');
            // snackbar('Item has added successfully in cart list.','success');
        }

        localStorage.setItem("products", JSON.stringify(productsInAddToCart));
        localStorage.setItem("ids", JSON.stringify(ids));
        JSON.parse(localStorage.getItem("ids"));

        dataCalculation();
    });


    $(document).on('click', '.remove', function () {
        var id = $(this).data('id');
        removeAddToCart(id);
        cardDetail()
        productSummmary()
    });

    window.cardDetail = function cardDetail() {
        var productsInAddToCart = JSON.parse(localStorage.getItem('products'));

        var html = '                                <table class="table-cart border-bottom">' +
            '                                    <thead>' +
            '                                        <tr>' +
            '                                            <th class="product-image"></th>' +
            '                                            <th class="product-name">Product</th>' +
            '                                            <th class="product-price d-none d-lg-table-cell">Per/Price</th>' +
            '                                            <th class="product-quanity d-none d-md-table-cell">Set of</th>' +
            '                                            <th class="product-total">Total</th>' +
            '                                            <th class="product-total">Quantity</th>' +
            '                                            <th class="product-total">Grand Total</th>' +
            '                                            <th class="product-remove">Action</th>' +
            '                                        </tr>' +
            '                                    </thead>' +
            '                                    <tbody>';

        //loop
        var base_url = $('.base_url').attr('href');
        var product_url = base_url + '/wholesale/product/';
        var image_url = base_url + '/';

        $.each(productsInAddToCart, function (i, res) {
            if (!['', undefined, null].includes(JSON.parse(res.photos))) {
                var image = JSON.parse(res.photos)[0]
            }
            html += '<tr class="cart-item" >' +
                '<td class="productd-image card-item-detail" width="60px"  data-id="' + res.id + '">' +
                '<img src="' + image_url + image + '" style="width: 100%"></td>' +
                '<td class="product-name card-item-detail"  data-id="' + res.id + '">' + res.name + '</td>' +
                '<td class="product-price">' + res.unit_price + '</td>' +
                '<td class="product-quantity">' + res.bulk.over_all_qty + '</td>' +
                '<td>' + res.bulk.per_set_price.toFixed(2) + '</td>' +
                '<td>' +
                '<div class="input-group input-group--style-2 pr-4" style="width: 130px;">' +

                '  <span class="input-group-btn">' +
                '   <button class="btn btn-number-plus minus" data-id="' + res.id + '" type="button" data-type="minus"' +
                '           data-field="quantity">' +
                '       <i class="la la-minus"></i>' +
                '   </button>' +
                ' </span>' +
                ' <input type="text" name="quantity"' +
                '      class="form-control h-auto input-number text-center"' +
                '      placeholder="1" value="' + res.bulk.no_of_sets + '" min="1" max="10" id="qty" value="' + res.bulk.no_of_sets + '">' +
                ' <span class="input-group-btn">' +
                '   <button class="btn btn-number-minus plus" data-id="' + res.id + '" type="button" data-type="plus"' +
                '           data-field="quantity">' +
                '       <i class="la la-plus"></i>' +
                '   </button>' +
                ' </span>' +
                '</div>' +
                '</td>' +
                '<td>' + (res.bulk.no_of_sets * res.bulk.per_set_price).toFixed(2) + '</td>' +
                '<td>' +
                '<a href="#" class="text-right pl-4 remove" data-id="' + res.id + '">' +
                '<i class="la la-trash"></i>' +
                '</a>' +
                '</td>' +
                '</t>'

            //loop end
        })


        html += '</tbody></table>'

        $('.cart-data').html(html)
    }


    // === QTY JS === //
    $(document).on('click', '.minus', function () {
        var product_id = $(this).data('id');
        var $input = $(this).parent().parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        if (!['', null, undefined].includes(product_id)) {
            updateQuantity(product_id, count)
        }
        dataCalculation()
        // cardDetail();
        return false;
    });

    $(document).on('click', '.plus', function (e) {
        e.preventDefault()
        var product_id = $(this).data('id');
        var $input = $(this).parent().parent().find('input');
        var count = parseInt($input.val()) + 1;
        $input.val(count);
        $input.change();

        if (!['', null, undefined].includes(product_id)) {
            updateQuantity(product_id, count)
        }
        dataCalculation();
        // cardDetail()
        return false;
    });

    function updateQuantity(product_id, number) {
        var p = JSON.parse(localStorage.getItem('products'));
        $.each(p, function (i, res) {
            if (res.id === product_id) {
                res.bulk.no_of_sets = number
            }
            return res;
        });

        localStorage.setItem("products", JSON.stringify(p));
        productSummmary();


    }


    function addItemNumberInCart(ids) {
        if (ids) {
            $('.add-to-cart-nav').text(Object.keys(ids).length);
            // $('.add-to-cart-text').text(Object.keys(ids).length > 1 ? 'Items' : 'Item');
        }
    }

    $(document).on('click', '.card-item-detail', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#product-detail').modal('show');
        productsInAddToCart = JSON.parse(localStorage.getItem('products'));
        var item = productsInAddToCart.find(res => res.id === id);

        var table = '<table class="table">' +
            '<thead><tr>' +
            '<th>Variant</th>' +
            '<th>Price</th>' +
            '<th>Quantity</th>' +
            '<th>Total</th>' +
            '</tr>' +
            '</thead><tbody>';

        if (!['', null, undefined].includes(item.stocks)) {
            var total_sum = 0;
            $.each(item.stocks, function (i, data) {
                if (data.selected === '1') {
                    var individual_sum = parseFloat(data.bulk_qty) * parseFloat(data.price)

                    // calculation
                    total_sum += individual_sum;
                    table += '<tr class="bulk-variant" ">' +
                        '<td>' + data.variant + '</td>' +
                        '<td>' + data.price + '</td>' +
                        '<td>' + data.bulk_qty + '</td>' +
                        '<td>' + individual_sum + '</td>' +
                        '</tr>';
                }

            });


        }
        // table += '<tr>' +
        //     '<td colspan="3">Total</td>' +
        //     '<td>' + total_sum + '</td>' +
        //     '</tr>';
        table += '</tbody></table>';


        $('#variants-detail').html(table);
    })

    function productSummmary() {
        var products = JSON.parse(localStorage.getItem('products'));


        var html = '<table class="table-cart table-cart-review">' +
            '            <thead>' +
            '            <tr>' +
            '                <th class="product-name">Product</th>' +
            '                <th class="product-name">Quantity</th>' +
            '                <th class="product-total text-right">Total</th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>';

        //loop

        var subtotal = 0;
        var grandTotal = 0;
        var tax = 0;
        var total_shipping = 0;

        if (products) {
            $('#summary-badge').html(products.length + ' Item' + products.length > 0 ? 's' : '');
            $.each(products, function (i, res) {
                tax += res.tax * res.bulk.no_of_sets * res.bulk.over_all_qty;
                total_shipping += res.shipping_cost;
                subtotal += (res.bulk.per_set_price * res.bulk.no_of_sets);

                html += '<tr class="cart_item">' +
                    '<td class="product-name">' + res.name +
                    '<strong class="product-quantity"> × ' + res.bulk.over_all_qty + '</strong>' +
                    '</td>' +
                    '<td class="product-name">' +
                    '<strong class="product-quantity"> × ' + res.bulk.no_of_sets + '</strong>' +
                    '</td>' +
                    '<td class="product-total text-right">' +
                    '             <span class="pl-4">Rs.' + (res.bulk.per_set_price * res.bulk.no_of_sets).toFixed(2) + '</span>' +
                    '             </td>' +
                    '        </tr>';
            });
        } else {
            $('#summary-badge').html('0 Item');
        }

        //end Loop

        grandTotal = subtotal + tax + total_shipping;
        localStorage.setItem("sub_total", subtotal);
        localStorage.setItem("grand_tatal", grandTotal);
        localStorage.setItem("tax", tax);
        localStorage.setItem("total_shipping", total_shipping);

        html += '</tbody>' +
            '        </table>' +
            '        <table class="table-cart table-cart-review">' +
            '            <tfoot>' +
            '            <tr class="cart-subtotal">' +
            '                <th>Sub Total</th>' +
            '                <td class="text-right">' +
            '                    <span class="strong-600">Rs.' + subtotal.toFixed(2) + '</span>' +
            '                </td>' +
            '            </tr>' +
            '            <tr class="cart-shipping">' +
            '                <th>Tax</th>' +
            '                <td class="text-right">' +
            '                    <span class="text-italic">' + tax.toFixed(2) + '</span>' +
            '                </td>' +
            '            </tr>' +
            '            <tr class="cart-shipping">' +
            '                <th>Total Shipping</th>' +
            '                <td class="text-right">' +
            '                    <span class="text-italic">' + total_shipping.toFixed(2) + '</span>' +
            '                </td>' +
            '            </tr>' +
            '            <tr class="cart-total">' +
            '                <th><span class="strong-600">Total</span></th>' +
            '                <td class="text-right">' +
            '                    <strong><span>Rs.' + grandTotal.toFixed(2) + '</span></strong>' +
            '                </td>' +
            '            </tr>' +
            '            </tfoot>' +
            '        </table>';


        $('#summary-card').html(html);


    }


})
