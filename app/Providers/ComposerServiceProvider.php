<?php

namespace App\Providers;

use App\BusinessSetting;
use App\Conversation;
use App\GeneralSetting;
use App\Language;
use App\Models\Appointment;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Specialist;
use App\Models\Test;
use App\Models\User;
use App\SeoSetting;
use App\Services\Admin\AppointmentService;
use App\Services\Admin\OrderService;
use App\Services\Admin\ServiceOfferedService;
use App\Services\Admin\SpecialistService;
use App\Services\Admin\UserService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('wholesale.layouts.app', function ($view) {
            $data['sliders'] = Slider::where('published', 1)->get();
            $data['seosetting'] = SeoSetting::first();
            $data['bussinessSettingFacbookChat'] = BusinessSetting::where('type', 'facebook_chat')->first();
            $data['bussinessSettingFacebookPixel'] = BusinessSetting::where('type', 'facebook_pixel')->first();
            $data['bussinessSettingGoogleAnalytics'] = BusinessSetting::where('type', 'google_analytics')->first();
            $data['languages'] = Language::all();
//            $data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
            $data['generalsetting'] = GeneralSetting::first();
            $view->with('slider', $data['sliders']);
        });



        //nurse
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
