<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulk extends Model
{
    protected $table = 'bulks';
    protected $guarded = ['id'];
    //

    public function getAttributeBulkQuantityDiscountPer($key)
    {
        return json_decode($key);
    }
}
