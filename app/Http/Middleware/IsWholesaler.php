<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsWholesaler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(\Illuminate\Support\Facades\Auth::user()->user_type);
        if (Auth::check() && (Auth::user()->user_type == 'wholesaler' || Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller')) {
            return $next($request);
        }
        else{
            flash(__('Please Login to see the further details'))->error();
            return redirect('/users/login');
        }
    }
}
