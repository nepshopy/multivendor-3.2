<?php

namespace App\Http\Controllers;


use App\Seller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;

class RequestLoginController extends Controller
{
    public function index(Request $request, $id)
    {
//        return $id;
        try {
            $id = decrypt($id);
            $user = User::find($id);
            if ($user && ($user->user_type === 'seller' ||  $user->user_type === 'wholesaler')) {
                auth()->login($user, true);
                return redirect()->route('dashboard');
            }
            return redirect('/users/login');
        } catch (Exception $e) {
            return redirect('/users/login');
        }

    }

}
