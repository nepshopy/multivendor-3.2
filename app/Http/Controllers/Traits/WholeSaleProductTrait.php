<?php

namespace App\Http\Controllers\Traits;

use App\Address;
use App\Brand;
use App\BusinessSetting;
use App\Color;
use App\Conversation;
use App\FlashDeal;
use App\GeneralSetting;
use App\Http\Controllers\InstamojoController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\PaystackController;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\RazorpayController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\TwoCheckoutController;
use App\Http\Controllers\VoguePayController;
use App\Models\Category;
use App\Language;
use App\Models\Slider;
use App\Order;
use App\Product;
use App\Seller;
use App\SeoSetting;
use App\Shop;
use App\SubCategory;
use App\SubSubCategory;
use App\Utility\PayhereUtility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait WholeSaleProductTrait
{
    protected $data = [];

    public function __construct(Request $request)
    {
        $this->data['sliders'] = Slider::where('published', 1)->get();
        $this->data['seosetting'] = SeoSetting::first();
        $this->data['bussinessSettingFacbookChat'] = BusinessSetting::where('type', 'facebook_chat')->first();
        $this->data['bussinessSettingFacebookPixel'] = BusinessSetting::where('type', 'facebook_pixel')->first();
        $this->data['bussinessSettingGoogleAnalytics'] = BusinessSetting::where('type', 'google_analytics')->first();
        $this->data['languages'] = Language::all();
        $this->data['generalsetting'] = GeneralSetting::first();
    }

    public function wholeSaleProduct(Request $request)
    {
        $this->data['conversation'] = [];
        if (Auth::user()) {
            $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        }

        $this->data['b'] = BusinessSetting::where('type', 'classified_product')->first();
        $this->data['flash_deal'] = FlashDeal::where('status', 1)->where('featured', 1)->first();

        $this->data['customer_products'] = \App\CustomerProduct::where('status', '1')->where('published', '1')->take(10)->get();
        $this->data['categories'] = Category::orderBy('created_at','desc')->take(11)->get();


        return view('wholesale.index', $this->data);
    }

    public function wholeSaleSearch(Request $request)
    {
        if (Auth::user()) {

            $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        }

        $query = $request->q;
        $brand_id = (Brand::where('slug', $request->brand)->first() != null) ? Brand::where('slug', $request->brand)->first()->id : null;
        $sort_by = $request->sort_by;
        $category_id = (\App\Category::where('slug', $request->category)->first() != null) ? Category::where('slug', $request->category)->first()->id : null;
        $subcategory_id = (SubCategory::where('slug', $request->subcategory)->first() != null) ? SubCategory::where('slug', $request->subcategory)->first()->id : null;
        $subsubcategory_id = (SubSubCategory::where('slug', $request->subsubcategory)->first() != null) ? SubSubCategory::where('slug', $request->subsubcategory)->first()->id : null;
        $min_price = $request->min_price;
        $max_price = $request->max_price;
        $seller_id = $request->seller_id;

        $conditions = ['published' => 1];
        $conditions = ['added_by' => 'wholesaler'];

        if ($brand_id != null) {
            $conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        }
        if ($category_id != null) {
            $conditions = array_merge($conditions, ['category_id' => $category_id]);
        }
        if ($subcategory_id != null) {
            $conditions = array_merge($conditions, ['subcategory_id' => $subcategory_id]);
        }
        if ($subsubcategory_id != null) {
            $conditions = array_merge($conditions, ['subsubcategory_id' => $subsubcategory_id]);
        }
        if ($seller_id != null) {
            $conditions = array_merge($conditions, ['user_id' => Seller::findOrFail($seller_id)->user->id]);
        }

        $products = Product::where($conditions);

        if ($min_price != null && $max_price != null) {
            $products = $products->where('unit_price', '>=', $min_price)->where('unit_price', '<=', $max_price);
        }

        if ($query != null) {
            $searchController = new SearchController;
            $searchController->store($request);
            $products = $products->where('name', 'like', '%' . $query . '%')->orWhere('tags', 'like', '%' . $query . '%');
        }

        if ($sort_by != null) {
            switch ($sort_by) {
                case '1':
                    $products->orderBy('created_at', 'desc');
                    break;
                case '2':
                    $products->orderBy('created_at', 'asc');
                    break;
                case '3':
                    $products->orderBy('unit_price', 'asc');
                    break;
                case '4':
                    $products->orderBy('unit_price', 'desc');
                    break;
                default:
                    // code...
                    break;
            }
        }

        $non_paginate_products = filter_products($products)->get();

        //Attribute Filter

        $attributes = array();
        foreach ($non_paginate_products as $key => $product) {
            if ($product->attributes != null && is_array(json_decode($product->attributes))) {
                foreach (json_decode($product->attributes) as $key => $value) {
                    $flag = false;
                    $pos = 0;
                    foreach ($attributes as $key => $attribute) {
                        if ($attribute['id'] == $value) {
                            $flag = true;
                            $pos = $key;
                            break;
                        }
                    }
                    if (!$flag) {
                        $item['id'] = $value;
                        $item['values'] = array();
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if ($choice_option->attribute_id == $value) {
                                $item['values'] = $choice_option->values;
                                break;
                            }
                        }
                        array_push($attributes, $item);
                    } else {
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if ($choice_option->attribute_id == $value) {
                                foreach ($choice_option->values as $key => $value) {
                                    if (!in_array($value, $attributes[$pos]['values'])) {
                                        array_push($attributes[$pos]['values'], $value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $selected_attributes = array();

        foreach ($attributes as $key => $attribute) {
            if ($request->has('attribute_' . $attribute['id'])) {
                foreach ($request['attribute_' . $attribute['id']] as $key => $value) {
                    $str = '"' . $value . '"';
                    $products = $products->where('choice_options', 'like', '%' . $str . '%');
                }

                $item['id'] = $attribute['id'];
                $item['values'] = $request['attribute_' . $attribute['id']];
                array_push($selected_attributes, $item);
            }
        }


        //Color Filter
        $all_colors = array();

        foreach ($non_paginate_products as $key => $product) {
            if ($product->colors != null) {
                foreach (json_decode($product->colors) as $key => $color) {
                    if (!in_array($color, $all_colors)) {
                        array_push($all_colors, $color);
                    }
                }
            }
        }

        $selected_color = null;

        if ($request->has('color')) {
            $str = '"' . $request->color . '"';
            $products = $products->where('colors', 'like', '%' . $str . '%');
            $selected_color = $request->color;
        }

        $products = filter_products($products)->paginate(12)->appends(request()->query());
        $this->data['products'] = $products;
        $this->data['query'] = $query;
        $this->data['category_id'] = $category_id;
        $this->data['subcategory_id'] = $subcategory_id;
        $this->data['subsubcategory_id'] = $subsubcategory_id;
        $this->data['brand_id'] = $brand_id;
        $this->data['sort_by'] = $sort_by;
        $this->data['seller_id'] = $seller_id;
        $this->data['min_price'] = $min_price;
        $this->data['attributes'] = $attributes;
        $this->data['selected_attributes'] = $selected_attributes;
        $this->data['all_colors'] = $all_colors;
        $this->data['selected_color'] = $selected_color;
        if (Auth::user()) {

            $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        }

        return view('wholesale.product_listing', $this->data);
    }

    public function wholeSaleAjaxSearch(Request $request)
    {
        $keywords = array();
        $products = Product::where('published', 1)->where('added_by','wholesaler')->where('tags', 'like', '%' . $request->search . '%')->get();
        foreach ($products as $key => $product) {
            foreach (explode(',', $product->tags) as $key => $tag) {
                if (stripos($tag, $request->search) !== false) {
                    if (sizeof($keywords) > 5) {
                        break;
                    } else {
                        if (!in_array(strtolower($tag), $keywords)) {
                            array_push($keywords, strtolower($tag));
                        }
                    }
                }
            }
        }

        $products = filter_products(Product::where('published', 1)->where('added_by','wholesaler')->where('name', 'like', '%' . $request->search . '%'))->get()->take(3);

        $subsubcategories = SubSubCategory::where('name', 'like', '%' . $request->search . '%')->get()->take(3);

        $shops = Shop::whereIn('user_id', verified_sellers_id())->where('name', 'like', '%' . $request->search . '%')->get()->take(3);

        if (sizeof($keywords) > 0 || sizeof($subsubcategories) > 0 || sizeof($products) > 0 || sizeof($shops) > 0) {
            return view('wholesale.partials.search_content', compact('products', 'subsubcategories', 'keywords', 'shops'));
        }
        return '0';
    }

    public function allCategories()
    {
        if (Auth::user()) {
            $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        }
        $this->data['categories'] = Category::all();
        return view('wholesale.all_category', $this->data);
    }

    public function productDetail(Request $request, $slug)
    {
        $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        $this->data['detailedProduct'] = Product::/*where('added_by','wholesaler')*/with([
            'bulk' => function ($query) {
                $query->selectRaw("product_id,color,bulk_attribute_ids,bulk_quantity_discount_per,'1' as over_all_qty,'1' as no_of_sets, '0' as per_set_price")->get();
            },
            'stocks' => function ($query) {
                $query->selectRaw("product_id,variant,sku,price,qty,manual_sku,rack_id,bulk_qty,'0' as selected")->get();
            }
        ])->where('slug', $slug)->first();
//        dd($this->data['detailedProduct']);
        $this->data['colorName'] = $this->data['detailedProduct'] ? Color::whereIn('code', json_decode($this->data['detailedProduct']->colors))->pluck('name') :'';
        $this->data['colorsCode'] = $this->data['detailedProduct'] ? Color::whereIn('code', json_decode($this->data['detailedProduct']->colors))->pluck('name', 'code'): '';

        if ($this->data['detailedProduct'] != null && $this->data['detailedProduct']->published) {
            if ($this->data['detailedProduct']->digital == 1) {
                return view('frontend.digital_product_details', $this->data);
            } else {
                return view('wholesale.bulk_product_details', $this->data);
            }
            // return view('frontend.product_details', compact('detailedProduct'));
        }
        abort(404);
    }

    public function cart(Request $request)
    {
        $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        $this->data['categories'] = Category::all();
        return view('wholesale.view_cart', $this->data);
    }

    public function checkout(Request $request)
    {
        $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();
        $this->data['categories'] = Category::all();
        return view('wholesale.shipping_info', $this->data);
    }

    public function store_shipping_info(Request $request)
    {
        if (Auth::check()) {
            $address = Address::findOrFail($request->address_id);
            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['address'] = $address->address;
            $data['country'] = $address->country;
            $data['city'] = $address->city;
            $data['postal_code'] = $address->postal_code;
            $data['phone'] = $address->phone;
            $data['checkout_type'] = $request->checkout_type;
        } else {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['address'] = $request->address;
            $data['country'] = $request->country;
            $data['city'] = $request->city;
            $data['postal_code'] = $request->postal_code;
            $data['phone'] = $request->phone;
            $data['checkout_type'] = $request->checkout_type;
        }

        $shipping_info = $data;
        $request->session()->put('shipping_info', $shipping_info);
//
//        $subtotal = 0;
//        $tax = 0;
//        $shipping = 0;

        $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();

        return view('wholesale.delivery_info', $this->data);
    }

    public function store_delivery_info()
    {
        $this->data['total'] = 1;
        //dd($total);
        $this->data['conversation'] = Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', '1')->get();

        return view('wholesale.payment_select', $this->data);

    }

    public function order_confirmed($oderId)
    {
        $order = Order::findOrFail($oderId);
//        dd($order->orderDetails->toArray());
        if (!$order) {
            return abort(404);
        }

        if (Auth::user()->id === $order->user_id) {
            return view('frontend.order_confirmed', compact('order'));
        }
        return abort(403);
    }
}
