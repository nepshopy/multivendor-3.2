<?php

Route::get('/product', 'ProductController@wholeSaleProduct')->name('wholesale.product.index');

Route::get('/search', 'ProductController@wholeSaleSearch')->name('wholesale.product.search');
Route::get('/search?category={category_slug}', 'ProductController@search')->name('wholesale.products.category');

Route::get('/search?q={search}', 'ProductController@wholeSaleSearch')->name('wholesale.suggestion.search');
Route::get('/products/sub-sub-category/{id}', 'Api\ProductController@subSubCategory')->name('wholesale.products.subSubCategory');

Route::get('/search', 'ProductController@wholeSaleSearch')->name('wholesale.product.search');
Route::post('/wholesale-ajax-search', 'ProductController@wholeSaleAjaxSearch')->name('wholesale.product.whole-sale-search');

Route::get('/categories', 'ProductController@allCategories')->name('wholesale.categories.all');


Route::group(['middleware' => ['wholeSaler']], function () {

    Route::get('/product/{slug}', 'ProductController@productDetail')->name('wholesale.product.detail');
    Route::get('/cart', 'ProductController@cart')->name('wholesale.cart');
    Route::get('/checkout', 'ProductController@checkout')->name('wholesale.checkout');
    Route::any('/checkout/delivery_info', 'ProductController@store_shipping_info')->name('wholesale.checkout.store_shipping_infostore');
    Route::post('/checkout/payment_select', 'ProductController@store_delivery_info')->name('wholesale.checkout.store_delivery_info');
    Route::get('/checkout/order-confirmed/{order_id}', 'ProductController@order_confirmed')->name('wholesale.order_confirmed');

});

