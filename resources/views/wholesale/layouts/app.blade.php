<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
    <html dir="rtl" lang="en">
    @else
        <html lang="en">
        @endif
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="robots" content="index, follow">
            <title>NBTC |@yield('meta_title', config('app.name', 'Nepal Bussiness Trade Centre'))</title>
            <meta name="description" content="@yield('meta_description', $seosetting->description)"/>
            <meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
            <meta name="author" content="{{ $seosetting->author }}">
            <meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">

        @yield('meta')

        @if(!isset($detailedProduct) && !isset($shop) && !isset($page))
            <!-- Schema.org markup for Google+ -->
                <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
                <meta itemprop="description" content="{{ $seosetting->description }}">
                <meta itemprop="image" content="{{ my_asset(\App\GeneralSetting::first()->logo) }}">

                <!-- Twitter Card data -->
                <meta name="twitter:card" content="product">
                <meta name="twitter:site" content="@publisher_handle">
                <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
                <meta name="twitter:description" content="{{ $seosetting->description }}">
                <meta name="twitter:creator" content="@author_handle">
                <meta name="twitter:image" content="{{ my_asset(\App\GeneralSetting::first()->logo) }}">

                <!-- Open Graph data -->
                <meta property="og:title" content="{{ config('app.name', 'Laravel') }}"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content="{{ route('home') }}"/>
                <meta property="og:image" content="{{ my_asset(\App\GeneralSetting::first()->logo) }}"/>
                <meta property="og:description" content="{{ $seosetting->description }}"/>
                <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>
                <meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
        @endif

        <!-- Favicon -->
            <link type="image/x-icon" href="{{ my_asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon"/>

            <!-- Fonts -->
{{--            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"--}}
{{--                  rel="stylesheet" media="none" onload="if(media!='all')media='all'">--}}

            <!-- Bootstrap -->
{{--            <link rel="stylesheet" href="{{ my_asset('frontend/css/bootstrap.min.css') }}" type="text/css" media="all">--}}

{{--            <!-- Icons -->--}}
            <link rel="stylesheet" href="{{ my_asset('frontend/css/font-awesome.min.css') }}" type="text/css"
                  media="none" onload="if(media!='all')media='all'">
            <link rel="stylesheet" href="{{ my_asset('frontend/css/line-awesome.min.css') }}" type="text/css"
                  media="none" onload="if(media!='all')media='all'">

{{--            <link type="text/css" href="{{ my_asset('frontend/css/bootstrap-tagsinput.css') }}" rel="stylesheet"--}}
{{--                  media="none" onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/jodit.min.css') }}" rel="stylesheet" media="none"--}}
{{--                  onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet"--}}
{{--                  media="none" onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/slick.css') }}" rel="stylesheet" media="all">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/xzoom.css') }}" rel="stylesheet" media="none"--}}
{{--                  onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/jssocials.css') }}" rel="stylesheet" media="none"--}}
{{--                  onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/jssocials-theme-flat.css') }}" rel="stylesheet"--}}
{{--                  media="none" onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('frontend/css/intlTelInput.min.css') }}" rel="stylesheet"--}}
{{--                  media="none" onload="if(media!='all')media='all'">--}}
{{--            <link type="text/css" href="{{ my_asset('css/spectrum.css')}}" rel="stylesheet" media="none"--}}
{{--                  onload="if(media!='all')media='all'">--}}

            <!-- Global style (main) -->
{{--            <link type="text/css" href="{{ my_asset('frontend/css/active-shop.css') }}" rel="stylesheet" media="all">--}}


{{--            <link type="text/css" href="{{ my_asset('frontend/css/main.css') }}" rel="stylesheet" media="all">--}}
            <link type="text/css" href="{{my_asset('css/all.css')}}" rel="stylesheet">

        @if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
            <!-- RTL -->
                <link type="text/css" href="{{ my_asset('frontend/css/active.rtl.css') }}" rel="stylesheet" media="all">
        @endif

        <!-- color theme -->
            <link href="{{ my_asset('frontend/css/colors/'.\App\GeneralSetting::first()->frontend_color.'.css')}}"
                  rel="stylesheet" media="all">

            <!-- Custom style -->
            <link type="text/css" href="{{ my_asset('frontend/css/custom-style.css') }}" rel="stylesheet" media="all">

            <!-- jQuery -->
{{--            <script src="{{ my_asset('frontend/js/vendor/jquery.min.js') }}"></script>--}}

            <script src="{{ my_asset('frontend/js/all.js') }}"></script>

        @if ($bussinessSettingGoogleAnalytics && $bussinessSettingGoogleAnalytics->value == 1)
            <!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

                <script>
                    window.dataLayer = window.dataLayer || [];

                    function gtag() {
                        dataLayer.push(arguments);
                    }

                    gtag('js', new Date());
                    gtag('config', '{{ env('TRACKING_ID') }}');
                </script>
        @endif

        @if ($bussinessSettingFacebookPixel &&  $bussinessSettingFacebookPixel->value == 1)
            <!-- Facebook Pixel Code -->
                <script>
                    !function (f, b, e, v, n, t, s) {
                        if (f.fbq) return;
                        n = f.fbq = function () {
                            n.callMethod ?
                                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                        };
                        if (!f._fbq) f._fbq = n;
                        n.push = n;
                        n.loaded = !0;
                        n.version = '2.0';
                        n.queue = [];
                        t = b.createElement(e);
                        t.async = !0;
                        t.src = v;
                        s = b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t, s)
                    }(window, document, 'script',
                        'https://connect.facebook.net/en_US/fbevents.js');
                    fbq('init', {{ env('FACEBOOK_PIXEL_ID') }});
                    fbq('track', 'PageView');
                </script>
                <noscript>
                    <img height="1" width="1" style="display:none"
                         src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}/&ev=PageView&noscript=1"/>
                </noscript>
                <!-- End Facebook Pixel Code -->
            @endif

        </head>
        <body>
        <a href="{{URL::to('/')}}" class="base_url"></a>


        <!-- MAIN WRAPPER -->
        <div class="body-wrap shop-default shop-cards shop-tech gry-bg">

            <!-- Header -->

            @include('wholesale.inc.bulk_nav')

            @yield('content')

            @include('wholesale.inc.footer')

            @include('wholesale.partials.modal')

            @if ($bussinessSettingFacbookChat && $bussinessSettingFacbookChat->value == 1)
                <div id="fb-root"></div>
                <!-- Your customer chat code -->
                <div class="fb-customerchat"
                     attribution=setup_tool
                     page_id="{{ env('FACEBOOK_PAGE_ID') }}">
                </div>
            @endif

            <div class="modal fade" id="addToCart">
                <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size"
                     role="document">
                    <div class="modal-content position-relative">
                        <div class="c-preloader">
                            <i class="fa fa-spin fa-spinner"></i>
                        </div>
                        <button type="button" class="close absolute-close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div id="addToCart-modal-body">

                        </div>
                    </div>
                </div>
            </div>

        </div><!-- END: body-wrap -->

        <!-- SCRIPTS -->
        <!-- <a href="#" class="back-to-top btn-back-to-top"></a> -->

{{--        <!-- Core -->--}}
{{--        <script src="{{ my_asset('frontend/js/vendor/popper.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/vendor/bootstrap.min.js') }}"></script>--}}

{{--        <!-- Plugins: Sorted A-Z -->--}}
{{--        <script src="{{ my_asset('frontend/js/jquery.countdown.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/select2.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/nouislider.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/sweetalert2.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/slick.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/jssocials.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/bootstrap-tagsinput.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/jodit.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/xzoom.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/fb-script.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/lazysizes.min.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/intlTelInput.min.js') }}"></script>--}}

{{--        <!-- App JS -->--}}
{{--        <script src="{{ my_asset('frontend/js/active-shop.js') }}"></script>--}}
{{--        <script src="{{ my_asset('frontend/js/main.js') }}"></script>--}}
{{--        <script src="{{my_asset('js/add_to_cart.js')}}"></script>--}}


        <script>
            function showFrontendAlert(type, message) {
                if (type == 'danger') {
                    type = 'error';
                }
                swal({
                    position: 'top-end',
                    type: type,
                    title: message,
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        </script>

        @foreach (session('flash_notification', collect())->toArray() as $message)
            <script>
                showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}');
            </script>
        @endforeach
        <script>

            $(document).ready(function () {
                $(document).on('mouseover', '.category-nav-element', function (e) {
                    if (!$(this).find('.sub-cat-menu').hasClass('loaded')) {
                        categoryElements($(this));
                    }
                })
                // $('.category-nav-element').each(function(i, el) {
                //     $(el).on('mouseover', function(){
                //         if(!$(el).find('.sub-cat-menu').hasClass('loaded')){
                //            categoryElements();
                //         }
                //     });
                // });
                function categoryElements($el) {
                    $.post('{{ route('category.elements') }}', {
                        _token: '{{ csrf_token()}}',
                        id: $el.data('id')
                    }, function (data) {
                        $el.find('.sub-cat-menu').addClass('loaded').html(data);
                    });
                }
            });

            $('#search').on('keyup', function () {
                search();
            });

            $('#search').on('focus', function () {
                search();
            });

            function search() {
                var search = $('#search').val();
                if (search.length > 0) {
                    $('body').addClass("typed-search-box-shown");

                    $('.typed-search-box').removeClass('d-none');
                    $('.search-preloader').removeClass('d-none');
                    $.post('{{ route('wholesale.product.whole-sale-search') }}', {
                        _token: '{{ @csrf_token() }}',
                        search: search
                    }, function (data) {
                        if (data == '0') {
                            // $('.typed-search-box').addClass('d-none');
                            $('#search-content').html(null);
                            $('.typed-search-box .search-nothing').removeClass('d-none').html('Sorry, nothing found for <strong>"' + search + '"</strong>');
                            $('.search-preloader').addClass('d-none');

                        } else {
                            $('.typed-search-box .search-nothing').addClass('d-none').html(null);
                            $('#search-content').html(data);
                            $('.search-preloader').addClass('d-none');
                        }
                    });
                } else {
                    $('.typed-search-box').addClass('d-none');
                    $('body').removeClass("typed-search-box-shown");
                }
            }


        </script>

        @yield('script')

        </body>
        </html>
