@extends('wholesale.layouts.app')

@section('content')

    <div id="page-content">
        <section class="slice-xs sct-color-2 border-bottom">
            <div class="container container-sm">
                <div class="row cols-delimited justify-content-center">
                    <div class="col">
                        <div class="icon-block icon-block--style-1-v5 text-center ">
                            <div class="block-icon c-gray-light mb-0">
                                <i class="la la-shopping-cart"></i>
                            </div>
                            <div class="block-content d-none d-md-block">
                                <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('1. My Cart')}}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="icon-block icon-block--style-1-v5 text-center ">
                            <div class="block-icon mb-0 c-gray-light">
                                <i class="la la-map-o"></i>
                            </div>
                            <div class="block-content d-none d-md-block">
                                <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('2. Shipping info')}}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="icon-block icon-block--style-1-v5 text-center active">
                            <div class="block-icon mb-0">
                                <i class="la la-truck"></i>
                            </div>
                            <div class="block-content d-none d-md-block">
                                <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('3. Delivery info')}}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="icon-block icon-block--style-1-v5 text-center">
                            <div class="block-icon c-gray-light mb-0">
                                <i class="la la-credit-card"></i>
                            </div>
                            <div class="block-content d-none d-md-block">
                                <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('4. Payment')}}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="icon-block icon-block--style-1-v5 text-center">
                            <div class="block-icon c-gray-light mb-0">
                                <i class="la la-check-circle"></i>
                            </div>
                            <div class="block-content d-none d-md-block">
                                <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">
                                    5. {{ translate('Confirmation')}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-4 gry-bg">
            <div class="container">
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col-xl-8">
                        <form class="form-default" data-toggle="validator"
                              action="{{ route('wholesale.checkout.store_delivery_info') }}" role="form" method="POST">
                            @csrf
                            <div class="card mb-3">
                                <div class="card-header bg-white py-3">
                                    <h5 class="heading-6 mb-0">{{ \App\GeneralSetting::first()->site_name }}
                                        Products</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-car-item">

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <label
                                                        class="d-flex align-items-center p-3 border rounded gry-bg c-pointer">
                                                        <input type="radio" name="shipping_type_admin"
                                                               value="home_delivery" checked class="d-none"
                                                               onchange="show_pickup_point(this)"
                                                               data-target=".pickup_point_id_admin">
                                                        <span class="radio-box"></span>
                                                        <span class="d-block ml-2 strong-600">
                                                            {{  translate('Home Delivery') }}
                                                        </span>
                                                    </label>
                                                </div>
                                                @if (\App\BusinessSetting::where('type', 'pickup_point')->first()->value == 1)
                                                    <div class="col-6">
                                                        <label
                                                            class="d-flex align-items-center p-3 border rounded gry-bg c-pointer">
                                                            <input type="radio" name="shipping_type_admin"
                                                                   value="pickup_point" class="d-none"
                                                                   onchange="show_pickup_point(this)"
                                                                   data-target=".pickup_point_id_admin">
                                                            <span class="radio-box"></span>
                                                            <span class="d-block ml-2 strong-600">
                                                                {{  translate('Local Pickup') }}
                                                            </span>
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>

                                            @if (\App\BusinessSetting::where('type', 'pickup_point')->first()->value == 1)
                                                <div class="mt-3 pickup_point_id_admin d-none">
                                                    <select class="pickup-select form-control-lg w-100"
                                                            name="pickup_point_id_admin"
                                                            data-placeholder="{{ translate('Select a pickup point') }}">
                                                        <option>{{ translate('Select your nearest pickup point')}}</option>
                                                        @foreach (\App\PickupPoint::where('pick_up_status',1)->get() as $key => $pick_up_point)
                                                            <option value="{{ $pick_up_point->id }}"
                                                                    data-address="{{ $pick_up_point->address }}"
                                                                    data-phone="{{ $pick_up_point->phone }}">
                                                                {{ $pick_up_point->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center pt-4">
                                <div class="col-md-6">
                                    <a href="{{ route('wholesale.product.index') }}" class="link link--style-3">
                                        <i class="ion-android-arrow-back"></i>
                                        {{ translate('Return to shop')}}
                                    </a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="submit"
                                            class="btn btn-styled btn-base-1">{{ translate('Continue to Payment')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 ml-lg-auto">
                        @include('frontend.partials.cart_summary')
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        productDetail();

        function productDetail() {
            //loop
            var products = JSON.parse(localStorage.getItem('products'));

            var html = '<table class="table-cart"><thead><tr><th></th><th>Title</th><th>Set Of</th><th>Ordered Qunatity</th></tr></thead>';
            var base_url = $('.base_url').attr('href');
            var product_url = base_url + '/wholesale/product/';
            var image_url = base_url + '/';

            $.each(products, function (i, res) {
                if (!['', undefined, null].includes(JSON.parse(res.photos))) {
                    var image = JSON.parse(res.photos)[0]
                }
                html +=
                    '  <tr class="cart-item">' +
                    '                 <td class="product-image" width="25%">' +
                    '                     <a href="' + product_url + res.slug + '" target="_blank">' +
                    '                         <img loading="lazy"  src="' + image_url + image + '">' +
                    '                     </a>' +
                    '                 </td>' +
                    '                 <td class="product-name strong-600">' +
                    '                     <a href="' + product_url + res.slug + '" target="_blank" class="d-block c-base-2">' + res.name +
                    '                     </a>' +
                    '                 </td>' +
                    '<td class="product-name strong-600">' + res.bulk.over_all_qty +
                    '                 </td>' +
                    '<td class="product-name strong-600" style="text-align: center">' + res.bulk.no_of_sets +
                    '                 </td>' +
                    '             </tr>';
            });


            html += '</tbody></table>';

            $('.table-car-item').html(html)
        }

        function display_option(key) {

        }

        function show_pickup_point(el) {
            var value = $(el).val();
            var target = $(el).data('target');

            console.log(value);

            if (value == 'home_delivery') {
                if (!$(target).hasClass('d-none')) {
                    $(target).addClass('d-none');
                }
            } else {
                $(target).removeClass('d-none');
            }
        }

    </script>
@endsection
