@if(count($combinations[0]) > 0)
	<table class="table table-bordered">
		<thead>
			<tr>
				<td class="text-center">
					<label for="" class="control-label">{{translate('Variant')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{translate('Variant Price')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{translate('Auto SKU')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{translate('Manual Sku')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">{{translate('Rack Id')}}</label>
				</td>
				<td class="text-center">
					<label for="" class="control-label">Stock Quantity</label>
				</td>
                <td class="text-center">
					<label for="" class="control-label">Bulk Quantity</label>
				</td>
			</tr>
		</thead>
		<tbody>


@foreach ($combinations as $key => $combination)
	@php
		$sku = '';
		foreach (explode(' ', $product_name) as $key => $value) {
			$sku .= substr($value, 0, 1);
		}

		$str = '';
		foreach ($combination as $key => $item){
			if($key > 0 ){
				$str .= '-'.str_replace(' ', '', $item);
				$sku .='-'.str_replace(' ', '', $item);
			}
			else{
				if($colors_active == 1){
				    $color =  \App\Color::where('code', $item)->first();
					$color_name = $color ? $color->name : '';
					$str .= $color_name;
					$sku .='-'.$color_name;
				}
				else{
					$str .= str_replace(' ', '', $item);
					$sku .='-'.str_replace(' ', '', $item);
				}
			}
		}
	@endphp
	@if(strlen($str) > 0)
			<tr>
				<td>
					<label for="" class="control-label">{{ $str }}</label>
				</td>
				<td>
					<input type="number" name="price_{{ $str }}" value="{{ $unit_price }}" min="0" step="0.01" class="form-control sku_price" required>
				</td>
				<td>
					<input type="text" name="sku_{{ $str }}" value="{{ $sku }}" class="form-control" required>
				</td>
				<td>
					<input type="text" name="manual_sku_{{ $str }}" placeholder="Put Value if needed" class="form-control">
				</td>
				<td>
					<input type="text" name="rack_id_{{ $str }}" placeholder="Put Value if needed" class="form-control">
				</td>
				<td>
					<input type="number" name="qty_{{ $str }}" value="1" min="0" step="1" class="form-control" required>
				</td>
                <td>
					<input type="number" name="bulk_qty_{{ $str }}" value="1" min="0" step="1" class="form-control" required>
				</td>
			</tr>
	@endif
@endforeach
	</tbody>
</table>
@endif
