@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{ translate('Add Your Product')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                                            <li><a href="{{ route('seller.products') }}">{{ translate('Products')}}</a>
                                            </li>
                                            <li class="active"><a
                                                    href="{{ route('seller.products.upload') }}">{{ translate('Add New Product')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{route('products.store')}}" method="POST" enctype="multipart/form-data"
                              id="choice_form">
                            @csrf
                            <input type="hidden" name="added_by" value="seller">

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('General')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Product Name')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" name="name"
                                                   placeholder="{{ translate('Product Name')}}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Product Category')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-control mb-3 c-pointer" data-toggle="modal"
                                                 data-target="#categorySelectModal"
                                                 id="product_category">{{ translate('Select a category')}}</div>
                                            <input type="hidden" name="category_id" id="category_id" value="" required>
                                            <input type="hidden" name="subcategory_id" id="subcategory_id" value=""
                                                   required>
                                            <input type="hidden" name="subsubcategory_id" id="subsubcategory_id"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Product Brand')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker"
                                                        data-placeholder="{{ translate('Select a brand') }}" id="brands"
                                                        name="brand_id">
                                                    <option value="">{{ ('Select Brand') }}</option>
                                                    @foreach (\App\Brand::all() as $brand)
                                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Product Unit')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" name="unit"
                                                   placeholder="{{ translate('Product unit') }}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Minimum Qty.')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" class="form-control mb-3" name="min_qty" value="1"
                                                   min="1" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Product Tag')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3 tagsInput" name="tags[]"
                                                   placeholder="{{ translate('Type & hit enter') }}"
                                                   data-role="tagsinput" required>
                                        </div>
                                    </div>
                                    @php
                                        $pos_addon = \App\Addon::where('unique_identifier', 'pos_system')->first();
                                    @endphp
                                    @if ($pos_addon != null && $pos_addon->activated == 1)
                                        <div class="row mt-2">
                                            <label class="col-md-2">{{ translate('Barcode')}}</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control mb-3" name="barcode"
                                                       placeholder="{{  translate('Barcode') }}">
                                            </div>
                                        </div>
                                    @endif

                                    @php
                                        $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
                                    @endphp
                                    @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                                        <div class="row mt-2">
                                            <label class="col-md-2">{{ translate('Refundable')}}</label>
                                            <div class="col-md-10">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input type="checkbox" name="refundable" checked>
                                                    <span class="slider round"></span></label>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4 d-none" id="displayStaticAtt">
                                <div class="form-box-title px-3 py-2">
                                    {{translate('Display Attributes')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="attribute-preloader text-center d-none ">
                                        <div class="loader">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="row d-none" id="displayStaticAttrib">
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Images')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div id="product-images">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('Main Images')}} <span
                                                        class="required-star">*</span></label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="file" name="photos[]" id="photos-1"
                                                       class="custom-input-file custom-input-file--4"
                                                       data-multiple-caption="{count} files selected" accept="image/*"/>
                                                <label for="photos-1" class="mw-100 mb-3">
                                                    <span></span>
                                                    <strong>
                                                        <i class="fa fa-upload"></i>
                                                        {{ translate('Choose image')}}
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-info mb-3"
                                                onclick="add_more_slider_image()">{{  translate('Add More') }}</button>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Thumbnail Image')}} <small>(290x300)</small> <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="thumbnail_img" id="file-2"
                                                   class="custom-input-file custom-input-file--4"
                                                   data-multiple-caption="{count} files selected" accept="image/*"/>
                                            <label for="file-2" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Videos')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Video From')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control selectpicker"
                                                        data-minimum-results-for-search="Infinity"
                                                        name="video_provider">
                                                    <option value="youtube">{{ translate('Youtube')}}</option>
                                                    <option value="dailymotion">{{ translate('Dailymotion')}}</option>
                                                    <option value="vimeo">{{ translate('Vimeo')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Video URL')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" name="video_link"
                                                   placeholder="{{ translate('Video link')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Meta Tags')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Meta Title')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" name="meta_title" class="form-control mb-3"
                                                   placeholder="{{ translate('Meta Title')}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Description')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea name="meta_description" rows="8"
                                                      class="form-control mb-3"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Meta Image')}} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="meta_img" id="file-5"
                                                   class="custom-input-file custom-input-file--4"
                                                   data-multiple-caption="{count} files selected" accept="image/*"/>
                                            <label for="file-5" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Customer Choice')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row mb-3 d-none">
                                        <div class="col-md-3">
                                            <label>{{__('Bulk Order')}}</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="col-4 col-xl-1 col-md-2 order-2 order-md-0 text-right">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input value="0" type="checkbox" name="bulk_order">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <p>Select any color to get the multi color image input.</p>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-8 col-md-3 order-1 order-md-0">
                                            <input type="text" class="form-control" value="{{ translate('Colors')}}"
                                                   disabled>
                                        </div>
                                        <div class="col-12 col-md-7 col-xl-8 order-3 order-md-0 mt-2 mt-md-0">
                                            <select class="form-control color-var-select" name="colors[]" id="colors"
                                                    multiple>
                                                @foreach (\App\Color::orderBy('name', 'asc')->get() as $key => $color)
                                                    <option value="{{ $color->code }}">{{ $color->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4 col-xl-1 col-md-2 order-2 order-md-0 text-right">
                                        <div class="d-none">
                                            <label class="switch" style="margin-top:5px;">
                                                <input value="1" type="checkbox" name="colors_active" checked>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                            @if(Auth::user()->user_type == 'wholesaler')
                                            <label class="switch bulk_switch" style="margin-top:5px;">
                                                <input type="checkbox" id="bulk-Check" name="color_bulk_check"
                                                       value="0">
                                                <span class="slider round"></span>
                                            </label>
                                            <span class="bulk_switch">Bulk</span>
                                                @endif
                                        </div>
                                    </div>
                                    <div id="product-attribute-images">
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{translate('Attribute Choice')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="mb-3">
                                        <p>Firstly choose category and all the attributes of this product and then only
                                            select variations of each attribute</p>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-3">
                                            <label>{{ translate('Attributes')}}</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="">
                                                <select name="choice_attributes[]" id="choice_attributes"
                                                        class="form-control selectpicker" multiple
                                                        data-placeholder="{{ translate('Select Category first and Choose Attributes') }}">
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <p>{{ translate('Choose the attributes of this product and then input values of each attribute') }}</p>
                                    </div>
                                    <div id="customer_choice_options">

                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-2">
                                         <button type="button" class="btn btn-info" onclick="add_more_customer_choice_option()">{{  translate('Add More Customer Choice Option') }}</button>
                                     </div>
                                 </div> --}}
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Price') }}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Unit Price') }} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3"
                                                   name="unit_price"
                                                   placeholder="{{ translate('Unit Price') }} ({{ translate('Base Price') }})"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Purchase Price') }} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3"
                                                   name="purchase_price" placeholder="{{ translate('Purchase Price')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Tax')}}</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3"
                                                   name="tax" placeholder="{{ translate('Tax') }}" required>
                                        </div>
                                        <div class="col-4 col-md-2">
                                            <div class="mb-3">
                                                <select class="form-control selectpicker" name="tax_type"
                                                        data-minimum-results-for-search="Infinity">
                                                    <option value="amount">$</option>
                                                    <option value="percent">%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Discount')}}</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3"
                                                   name="discount" placeholder="{{ translate('Discount') }}" required>
                                        </div>
                                        <div class="col-4 col-md-2">
                                            <div class="mb-3">
                                                <select class="form-control selectpicker" name="discount_type"
                                                        data-minimum-results-for-search="Infinity">
                                                    <option value="amount">$</option>
                                                    <option value="percent">%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="quantity">
                                        <div class="col-md-2">
                                            <label>{{ translate('Quantity') }} <span
                                                    class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="1" class="form-control mb-3"
                                                   name="current_stock" placeholder="{{ translate('Quantity') }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12" id="sku_combination">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (\App\BusinessSetting::where('type', 'shipping_type')->first()->value == 'product_wise_shipping')
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{ translate('Shipping') }}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('Flat Rate') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" min="0" step="0.01" value="0"
                                                       class="form-control mb-3" name="flat_shipping_cost"
                                                       placeholder="{{ translate('Flat Rate Cost') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input type="radio" name="shipping_type" value="flat_rate" checked>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('Free Shipping') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" min="0" step="0.01" value="0"
                                                       class="form-control mb-3" name="free_shipping_cost" value="0"
                                                       disabled placeholder="{{ translate('Flat Rate Cost') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input type="radio" name="shipping_type" value="free" checked>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(Auth::user()->user_type == 'wholesaler')
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    Bulk Price Discount
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        {{--                                        <div class="col-md-3">--}}
                                        {{--                                            <label>Bulk Price <span--}}
                                        {{--                                                    class="required-star">*</span></label>--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <div class="col-md-9">--}}
                                        {{--                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3"--}}
                                        {{--                                                   name="bulk_price"--}}
                                        {{--                                                   placeholder="Enter Bulk Price"--}}
                                        {{--                                                   required>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-md-12">
                                            <div class="multi-field-wrapper">
                                                <div class="multi-fields">
                                                    <div class="multi-field">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label><input type="number" name="start[]"
                                                                              style="width: 40px" value="1"/> <span
                                                                        style="font-size: 25px"> - </span> <input
                                                                        type="number" name="to[]" class="to-number"
                                                                        style="width: 40px"/>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4 ">
                                                                <label class="mt-3"> Discount in Percentage:</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" value="0" step="0.01"
                                                                       class="form-control mb-3"
                                                                       name="bulk_discount[]"
                                                                       placeholder="Discount in Percentage"
                                                                       required>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button"
                                                                        class="btn btn-info mb-3 remove-field"><i
                                                                        class="fa fa-remove"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-content p-3">

                                    <div class="mb-3">
                                        <div class="row mb-3">
                                            <div class="col-12 col-md-7 col-xl-8 order-3 order-md-0 mt-2 mt-md-0">
                                                <p>Do you want to sell this product as single ?</p>
                                            </div>
                                            <div class="col-4 col-xl-1 col-md-2 order-2 order-md-0 text-right">
                                                <label class="switch bulk_switch" style="margin-top:5px;">
                                                    <input type="checkbox" id="bulk-Check" name="product_display"
                                                           value="0">
                                                    <span class="slider round"></span>
                                                </label>
                                                <span class="bulk_switch"></span>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            @endif

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Description')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Description')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <textarea class="editor" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('PDF Specification')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('PDF')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="pdf" id="file-6"
                                                   class="custom-input-file custom-input-file--4"
                                                   data-multiple-caption="{count} files selected" accept="pdf/*"/>
                                            <label for="file-6" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose PDF')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box mt-4 text-right">
                                <button type="submit"
                                        class="btn btn-styled btn-base-1">{{  translate('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="categorySelectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">{{ translate('Select Category') }}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="target-category heading-6">
                        <span class="mr-3">{{ translate('Target Category')}}:</span>
                        <span>{{ translate('category')}} > {{ translate('subcategory')}} > {{ translate('subsubcategory')}}</span>
                    </div>
                    <div class="row no-gutters modal-categories mt-4 mb-2">
                        <div class="col-4">
                            <div class="modal-category-box c-scrollbar">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text"
                                               placeholder="{{ translate('Search Category') }}"
                                               onkeyup="filterListItems(this, 'categories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list has-right-arrow">
                                    <ul id="categories">
                                        @foreach ($categories as $key => $category)
                                            <li onclick="get_subcategories_by_category(this, {{ $category->id }})">{{  __($category->name) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="search-preloader text-center d-none ">
                                <div class="loader">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                            <div class="modal-category-box c-scrollbar" id="subcategory_list">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text"
                                               placeholder="{{ translate('Search SubCategory') }}"
                                               onkeyup="filterListItems(this, 'subcategories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list has-right-arrow">
                                    <ul id="subcategories">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="search-preloader-cat text-center d-none ">
                                <div class="loader">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                            <div class="modal-category-box c-scrollbar" id="subsubcategory_list">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text"
                                               placeholder="{{ translate('Search SubSubCategory') }}"
                                               onkeyup="filterListItems(this, 'subsubcategories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list">
                                    <ul id="subsubcategories">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ translate('cancel')}}</button>
                    <button type="button" class="btn btn-primary"
                            onclick="closeModal()">{{ translate('Confirm')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">

        function sumPrice() {
            var sum = 0;
            $('.sku_price').each(function (index, data) {
                sum += parseFloat($(this).val());
            })
            $("input[name=bulk_price]").val(sum);
        }

        $(document).on('keyup', '.sku_price', function (e) {
            e.preventDefault();
        })

        function addWrapper($wrapper, $this) {
            $(".add-field", $this).click(function (e) {
                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
            });
        }

        $('.multi-field-wrapper').each(function (index, data) {
            var $wrapper = $('.multi-fields', this);
            var $that = $(this);
            addWrapper($wrapper, $that);
            $(document).on('click', '.multi-field .remove-field', $wrapper, function () {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent().parent().parent('.multi-field').remove();
            });
            $(document).on('change', '.to-number', function (e) {
                e.preventDefault();

                var endVal = $('.multi-field:last-child').find('.to-number').val();
                // $('.num-start:last-child').val(number);

                if (!['', null, undefined].includes(endVal)) {
                    var number = parseInt($(this).val()) + 1;
                    var html = '<div class="multi-field">' +
                        '                    <div class="row">' +
                        '                    <div class="col-md-3">' +
                        '                        <label><input type="number" class="num-start" name="start[]" value="' + number + '"' +
                        '                                      style="width: 40px"/> <span' +
                        '                                style="font-size: 25px"> - </span> <input' +
                        '                                type="number"  name="to[]" class="to-number"' +
                        '                                style="width: 40px"/>' +
                        '                        </label>' +
                        '                    </div>' +
                        '                  <div class="col-md-4 ">' +
                        '<label class="mt-3"> Discount in Percentage:</label>' +
                        '</div>' +
                        '<div class="col-md-4">' +
                        '                        <input type="number" min="0" value="0" step="0.01"' +
                        '                               class="form-control mb-3"' +
                        '                               name="bulk_discount[]"' +
                        '                               placeholder="Price"' +
                        '                               required>' +
                        '                    </div>' +
                        '               <div class="col-md-1">' +
                        '                   <button type="button" class="btn btn-info mb-3 remove-field"><i' +
                        '                           class="fa fa-remove"></i>' +
                        '                   </button>' +
                        '               </div>'
                    '        </div>' +
                    '            </div>'

                    $('.multi-fields').append(html);
                }
                // $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
            })
        });

        var category_name = "";
        var subcategory_name = "";
        var subsubcategory_name = "";

        var category_id = null;
        var subcategory_id = null;
        var subsubcategory_id = null;

        $(document).ready(function () {
            $('#subcategory_list').hide();
            $('#subsubcategory_list').hide();
        });

        function list_item_highlight(el) {
            $(el).parent().children().each(function () {
                $(this).removeClass('selected');
            });
            $(el).addClass('selected');
        }

        function get_subcategories_by_category(el, cat_id) {
            list_item_highlight(el);
            category_id = cat_id;
            subcategory_id = null;
            subsubcategory_id = null;
            category_name = $(el).html();
            $('#subcategory_list').hide();
            $('#subcategories').html(null);
            $('#subsubcategory_list').hide();
            $('.search-preloader').removeClass('d-none');
            $.post('{{ route('subcategories.get_subcategories_by_category') }}', {
                _token: '{{ csrf_token() }}',
                category_id: category_id
            }, function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#subcategories').append('<li onclick="get_subsubcategories_by_subcategory(this, ' + data[i].id + ')">' + data[i].name + '</li>');
                }
                $('.search-preloader').addClass('d-none');
                $('#subcategory_list').show();
            });
        }

        function get_subsubcategories_by_subcategory(el, subcat_id) {
            list_item_highlight(el);
            subcategory_id = subcat_id;
            subsubcategory_id = null;
            subcategory_name = $(el).html();
            $('#subsubcategory_list').hide();
            $('#subsubcategories').html(null);
            $('.search-preloader-cat').removeClass('d-none');
            $.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}', {
                _token: '{{ csrf_token() }}',
                subcategory_id: subcategory_id
            }, function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#subsubcategories').append('<li onclick="confirm_subsubcategory(this, ' + data[i].id + ')">' + data[i].name + '</li>');
                }
                $('.search-preloader-cat').addClass('d-none');
                $('#subsubcategory_list').show();
            });
        }

        function confirm_subsubcategory(el, subsubcat_id) {
            list_item_highlight(el);
            subsubcategory_id = subsubcat_id;
            subsubcategory_name = $(el).html();
        }

        function get_attributes_by_subsubcategory(subsubcategory_id) {
            $.post('{{ route('subsubcategories.get_attributes_by_subsubcategory') }}', {
                _token: '{{ csrf_token() }}',
                subsubcategory_id: subsubcategory_id
            }, function (data) {
                $('#choice_attributes').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#choice_attributes').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                }
            });
        }

        function filterListItems(el, list) {
            filter = el.value.toUpperCase();
            li = $('#' + list).children();
            for (i = 0; i < li.length; i++) {
                if ($(li[i]).html().toUpperCase().indexOf(filter) > -1) {
                    $(li[i]).show();
                } else {
                    $(li[i]).hide();
                }
            }
        }

        function closeModal() {
            if (category_id > 0 && subcategory_id > 0) {
                $('#category_id').val(category_id);
                $('#subcategory_id').val(subcategory_id);
                $('#subsubcategory_id').val(subsubcategory_id);
                $('#product_category').html(category_name + '>' + subcategory_name + '>' + subsubcategory_name);
                $('#categorySelectModal').modal('hide');
                searchAttributes();
                add_attribute_option();
            } else {
                alert('Please choose categories...');
                // console.log(category_id);
                // console.log(subcategory_id);
                // console.log(subsubcategory_id);
            }
        }

        //var i = 0;

        function searchAttributes() {
            $("#choice_attributes").empty();
            $("#customer_choice_options").empty();
            update_sku();
            $("#displayStaticAttrib").empty();
            var cat_id = category_id;
            // console.log (category_id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',

                url: '/seller/search-attribute/',
                data: {'cat_id': cat_id},
                success: function (list) {
                    var veriation = list;
                    // console.log(list);

                    for (var i = 0; i < list.length; i++) {
                        if (list[i].att_p == 1) {
                            $('#choice_attributes').append('<option value="' + list[i].id + '">' + list[i].name + '</option>');
                        }
                    }
                }
            });
        }

        function add_attribute_option(response, veriation) {
            var sub_sub_cat_id = subsubcategory_id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                beforeSend: function () {
                    $('#displayStaticAtt').removeClass('d-none');
                    $('.attribute-preloader').removeClass('d-none');
                },
                url: '/seller/search-static-variations/',
                data: {'sub_sub_cat_id': sub_sub_cat_id},
                success: function (response) {
                    // console.log(response);
                    if (response) {
                        for (var i = 0; i < response.length; i++) {
                            if (response[i].var_name != "") {
                                var veriation = response[i].var_name;
                                $('#displayStaticAttrib').append('<div class="col-sm-6 row"><div class="col-md-6 text-right"><input type="hidden" name="att_prop_' + response[i].att_id + '[]" value="1"><input type="hidden" name="upper_choice_no[]" value="' + response[i].att_id + '"><input type="text" class="form-control" name="upper_choice[]" value="' + response[i].att_name + '" placeholder="Choice Title" readonly></div><div class="col-md-6"><div class="mb-3"><select name="upper_choice_options_' + response[i].att_id + '[]" id="upper_choice_variation_' + response[i].att_id + '" autocomplete = "off" class="form-control selectpicker" data-placeholder="Choose Variations"><option value="">{{__('Select the Variations') }}</option></select></div></div></div>');
                                for (var j = 0; j < veriation.length; j++) {
                                    $('#upper_choice_variation_' + response[i].att_id).append($('<option>', {
                                        value: veriation[j],
                                        text: veriation[j]
                                    }));
                                }
                            } else {
                                $('#displayStaticAttrib').append('<div class="col-sm-6 row"><div class="col-md-6 text-right"><input type="hidden" name="att_prop_' + response[i].att_id + '[]" value="0"><input type="hidden" name="upper_choice_no[]" value="' + response[i].att_id + '"><input type="text" class="form-control" name="upper_choice[]" value="' + response[i].att_name + '" readonly></div><div class="col-md-6"><div class="mb-3"><input type="text" class="form-control" name="upper_choice_options_' + response[i].att_id + '[]" placeholder="Enter choice values"></div></div></div>');

                            }
                        }
                    } else {
                        $('#displayStaticAtt').addClass('d-none');
                    }
                    // i++;
                    $('.attribute-preloader').addClass('d-none');
                    $('#displayStaticAttrib').removeClass('d-none');
                },
            });
        }

        function add_more_customer_choice_option(i, name, response, veriation) {
            if (category_id > 0) {
                var att_id = i;
                var cat_id = category_id;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'GET',

                    url: '/seller/search-variation/',
                    data: {'att_id': att_id, 'cat_id': cat_id},
                    success: function (response) {
                        // console.log(response);

                        if (response != null) {
                            var veriation = response;
                        } else {
                            var veriation = "";
                        }
                        $('#customer_choice_options').append('<div class="row mb-3"><div class="col-8 col-md-3 order-1 order-md-0"><input type="hidden" name="choice_no[]" value="' + i + '"><input type="text" class="form-control" name="choice[]" value="' + name + '" placeholder="Choice Title" readonly></div><div class="col-12 col-md-6 col-xl-7 order-3 order-md-0 mt-2 mt-md-0"><select name="choice_options_' + i + '[]" id="choice_variation_' + i + '" required class="form-control selectpicker" multiple data-placeholder="Choose Variations" onchange="update_sku()"></select></div><div class="col-4 col-xl-2 col-md-3 order-2 order-md-0 text-right"><label class="switch bulk_switch" style="margin-top:5px;"><input type="checkbox" value="' + i + '" name="bulk_attibute_check[]"><span class="slider round"></span></label><span class="bulk_switch">Bulk</span></div></div>');
                        for (var j = 0; j < veriation.length; j++) {
                            $('#choice_variation_' + i).append($('<option>', {
                                value: veriation[j],
                                text: veriation[j]
                            }));
                        }
                        // i++;
                        $('.tagsInput').tagsinput('items');
                    },
                });

            } else {
                alert('Please choose categories first...');
                $("#choice_attributes").prop('selectedIndex', -1)
                event.stopPropagation();
                event.preventDefault();
            }
        }

        $('input[name="colors_active"]').on('change', function () {
            if (!$('input[name="colors_active"]').is(':checked')) {
                $('#colors').prop('disabled', true);
            } else {
                $('#colors').prop('disabled', false);
            }
            update_sku();
        });

        $('input[name="bulk_order"]').on('change', function () {
            if ($('input[name="bulk_order"]').is(':checked')) {
                $('.bulk_switch').removeClass('d-none');
            } else {
                $('.bulk_switch').addClass('d-none');
            }
        });

        $('#colors').on('change', function () {
            update_sku();
            color_slider_image($(this).val());
        });

        $('input[name="unit_price"]').on('keyup', function () {
            update_sku();
        });

        $('input[name="name"]').on('keyup', function () {
            update_sku();
        });

        $('#choice_attributes').on('change', function () {
            $('#customer_choice_options').html(null);
            $.each($("#choice_attributes option:selected"), function () {
                add_more_customer_choice_option($(this).val(), $(this).text());
            });
            update_sku();
        });

        function delete_row(em) {
            $(em).closest('.row').remove();
            update_sku();
        }

        function update_sku() {
            $.ajax({
                type: "POST",
                url: '{{ route('products.sku_combination') }}',
                data: $('#choice_form').serialize(),
                success: function (data) {
                    $('#sku_combination').html(data);
                    if (data.length > 1) {
                        $('#quantity').hide();
                    } else {
                        $('#quantity').show();
                    }
                }
            });
        }

        var photo_id = 2;

        function add_more_slider_image() {
            var photoAdd = '<div class="row">';
            photoAdd += '<div class="col-2">';
            photoAdd += '<button type="button" onclick="delete_this_row(this)" class="btn btn-link btn-icon text-danger"><i class="fa fa-trash-o"></i></button>';
            photoAdd += '</div>';
            photoAdd += '<div class="col-10">';
            photoAdd += '<input type="file" name="photos[]" id="photos-' + photo_id + '" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" multiple accept="image/*" />';
            photoAdd += '<label for="photos-' + photo_id + '" class="mw-100 mb-3">';
            photoAdd += '<span></span>';
            photoAdd += '<strong>';
            photoAdd += '<i class="fa fa-upload"></i>';
            photoAdd += "{{ translate('Choose image')}}";
            photoAdd += '</strong>';
            photoAdd += '</label>';
            photoAdd += '</div>';
            photoAdd += '</div>';
            $('#product-images').append(photoAdd);

            photo_id++;
            imageInputInitialize();
        }

        function delete_this_row(em) {
            $(em).closest('.row').remove();
        }

        var color_photo_id = 20;

        function color_slider_image(colo) {
            var color = colo;
            // console.log(color);
            $('#product-attribute-images').empty();
            if (color != null) {
                for (var j = 0; j < color.length; j++) {
                    var photoAdd = '<label>{{__('Main Images For Color')}} <span class="required-star">*</span></label><div class="row mb-3">';
                    photoAdd += '<div class="container col-8 col-md-3 order-1 order-md-0"><div style="background: ' + color[j] + ';height: 70%;" class="col-12 col-md-12 order-1 order-md-0">';
                    photoAdd += '</div></div>';
                    photoAdd += '<div class="col-md-9 order-3 order-md-0 mt-2 mt-md-0">';
                    photoAdd += '<input type="hidden" name="attribute_image_color[]" value="' + color[j] + '"></input>';
                    photoAdd += '<input type="file" name="attribute_image[]" id="color_photos-' + color_photo_id + '" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" multiple accept="image/*" />';
                    photoAdd += '<label for="color_photos-' + color_photo_id + '" class="mw-100 mb-3">';
                    photoAdd += '<span></span>';
                    photoAdd += '<strong>';
                    photoAdd += '<i class="fa fa-upload"></i>';
                    photoAdd += "{{__('Choose image')}}";
                    photoAdd += '</strong>';
                    photoAdd += '</label>';
                    photoAdd += '</div>';
                    $('#product-attribute-images').append(photoAdd);

                    color_photo_id++;
                }
            }
        }

    </script>
@endsection
