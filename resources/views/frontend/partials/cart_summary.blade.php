<div class="card sticky-top">
    <div class="card-title py-3">
        <div class="row align-items-center">
            <div class="col-6">
                <h3 class="heading heading-3 strong-400 mb-0">
                    <span>{{translate('Summary')}}</span>
                </h3>
            </div>

            <div class="col-6 text-right">
                <span
                    class="badge badge-md badge-success" id="summary-badge">0 {{translate('Items')}}</span>
            </div>
        </div>
    </div>

    <div class="card-body" id="summary-card">
{{--        <table class="table-cart table-cart-review">--}}
{{--            <thead>--}}
{{--            <tr>--}}
{{--                <th class="product-name">{{translate('Product')}}</th>--}}
{{--                <th class="product-total text-right">{{translate('Total')}}</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--            <tr class="cart_item">--}}
{{--                <td class="product-name">--}}
{{--                    product with no choice--}}
{{--                    <strong class="product-quantity">× ddd</strong>--}}
{{--                </td>--}}
{{--                <td class="product-total text-right">--}}
{{--                    <span class="pl-4">ddd</span>--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--            </tbody>--}}
{{--        </table>--}}

{{--        <table class="table-cart table-cart-review">--}}

{{--            <tfoot>--}}
{{--            <tr class="cart-subtotal">--}}
{{--                <th>{{translate('Subtotal')}}</th>--}}
{{--                <td class="text-right">--}}
{{--                    <span class="strong-600">dd</span>--}}
{{--                </td>--}}
{{--            </tr>--}}

{{--            <tr class="cart-shipping">--}}
{{--                <th>{{translate('Tax')}}</th>--}}
{{--                <td class="text-right">--}}
{{--                    <span class="text-italic">ddd</span>--}}
{{--                </td>--}}
{{--            </tr>--}}

{{--            <tr class="cart-shipping">--}}
{{--                <th>{{translate('Total Shipping')}}</th>--}}
{{--                <td class="text-right">--}}
{{--                    <span class="text-italic">dd</span>--}}
{{--                </td>--}}
{{--            </tr>--}}

{{--            <tr class="cart-total">--}}
{{--                <th><span class="strong-600">{{translate('Total')}}</span></th>--}}
{{--                <td class="text-right">--}}
{{--                    <strong><span>ddd</span></strong>--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--            </tfoot>--}}
{{--        </table>--}}
{{--        @if (Auth::check() && \App\BusinessSetting::where('type', 'coupon_system')->first()->value == 1)--}}
{{--            @if (Session::has('coupon_discount'))--}}
{{--                <div class="mt-3">--}}
{{--                    <form class="form-inline" action="{{ route('checkout.remove_coupon_code') }}" method="POST"--}}
{{--                          enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="form-group flex-grow-1">--}}
{{--                            <div--}}
{{--                                class="form-control bg-gray w-100">{{ \App\Coupon::find(Session::get('coupon_id'))->code }}</div>--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="btn btn-base-1">{{translate('Change Coupon')}}</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            @else--}}
{{--                <div class="mt-3">--}}
{{--                    <form class="form-inline" action="{{ route('checkout.apply_coupon_code') }}" method="POST"--}}
{{--                          enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="form-group flex-grow-1">--}}
{{--                            <input type="text" class="form-control w-100" name="code"--}}
{{--                                   placeholder="{{translate('Have coupon code? Enter here')}}" required>--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="btn btn-base-1">{{translate('Apply')}}</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        @endif--}}


    </div>
</div>
